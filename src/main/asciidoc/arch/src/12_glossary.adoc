[[section-glossary]]
== Glossary





[options="header"]
|===
| Term | Long | German | Description
|AC|Architecture Committee|Architektur-Komittee|Gives framework and constraints according to architecture for oK projects.
|DSO|Distribution System Operator|Verteilnetzbetreiber (VNB)|Manages the distribution network for energy, gas or water.
|EPL|Eclipse Public License||Underlying license model for Eclipse projects like statement-public-affairs@openK
|QC|Quality Committee|Qualitätskomitee|Gives framework and constraints according to quality for oK projects.
|SPA| Statement Public Affairs | Stellungnahmen öffentlicher Belange | 
|===
