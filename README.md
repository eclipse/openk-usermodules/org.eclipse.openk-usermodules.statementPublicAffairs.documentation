# Eclipse openK User Modules - Statement Public Affairs documentation

This application represents a user module for the [Eclipse openK User Modules](https://projects.eclipse.org/projects/technology.openk-usermodules) project.

## Build

Run `mvn clean build` to build the documentation. All build artifacts will be stored in the `targets/` directory.


